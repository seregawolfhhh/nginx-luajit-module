FROM debian:9 as build

RUN apt update \
        && apt install -y wget gcc make \
        && apt install -y libpcre++-dev \
        && apt install -y libssl-dev \
        && apt install -y zlib1g-dev
RUN mkdir /usr/local/LuaJIT \
        && cd /usr/local/LuaJIT \
        && wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20210510.tar.gz \
        && tar -C $(pwd) -xzvf v2.1-20210510.tar.gz \
        && cd luajit2-2.1-20210510 \
        && make \
        && make install
RUN cd /usr/local \
        && wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.22.tar.gz \
        && tar -xzvf v0.1.22.tar.gz \
        && cd lua-resty-core-0.1.22 \
        && make install
RUN cd /usr/local \
        && wget https://github.com/openresty/lua-resty-lrucache/archive/v0.06.tar.gz \
        && tar -C $(pwd) -xzvf v0.06.tar.gz \
        && cd lua-resty-lrucache-0.06 \
        && make install
RUN cd /usr/local \
        && wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz \
        && tar -C $(pwd) -xzvf v0.3.1.tar.gz
RUN cd /usr/local \
        && wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.20.tar.gz \
        && tar -C $(pwd) -xzvf v0.10.20.tar.gz
RUN wget https://nginx.org/download/nginx-1.14.2.tar.gz \
        && tar -C $(pwd) -xzvf nginx-1.14.2.tar.gz \
        && cd nginx-1.14.2 \
        && export LUAJIT_LIB=/usr/local/lib \
        && export LUAJIT_INC=/usr/local/include/luajit-2.1 \
        && ./configure --with-ld-opt="-Wl,-rpath,/usr/local/lib" \
        --add-module=/usr/local/ngx_devel_kit-0.3.1 \
        --add-module=/usr/local/lua-nginx-module-0.10.20 \
        && make \
        && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir /usr/local/nginx/lib
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /lib
COPY --from=build /usr/local/lib/lua/resty/ /usr/local/lib/lua/resty/
RUN mkdir ../logs ../conf \
        && touch ../logs/error.log \
        && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
